# rule.py

Make a rule to use in text based contexts, like a shell terminal. It accepts the size of the rule as an argument; the default is the size of the terminal.

## Install

This a Python script, so it depends on the corresponding interpreter. It should operate on both versions 2.x and 3.x of Python. You must assure the interpreter is installed.

The generated rule only makes sense with monospaced type fonts.

For Unix like systems download the `rule.py` file and make it executable. You can also put the file in a PATH directory.
```console
$ chmod +x rule.py
$ sudo cp rule.py /usr/local/bin/
```

## Execute

On Unix like systems you can execute it like bellow.
```console
$ ./rule.py 32
....+....1....+....2....+....3..
$ ./rule.py --help

usage: rule.py [-h] [-l LEADING] [-v] [size]

Print a rule

positional arguments:
  size                  the size of rule

optional arguments:
  -h, --help            show this help message and exit
  -l LEADING, --leading LEADING
                        number of leading spaces
  -v, --vertical        vertical rule
```
On all systems you can execute it as bellow.
```console
$ python rule.py 32
....+....1....+....2....+....3..
```

## Import

You can also use it in your own Python scripts.
```python
$ python
>>> import rule
>>> print(rule.rule(32))
....+....1....+....2....+....3..
>>> print(rule.verticalrule(12))
.
.
.
.
+
.
.
.
.
1
.
.
```
