#!/usr/bin/env python
"""rule - make a text rule

Copyright (c) 2017, 2018 Alvaro Figueiredo
"""


from __future__ import print_function, unicode_literals
import argparse
import curses


def rule(size=80, leading=0):
    """Make an horizontal rule"""
    str_leading = leading * ' '
    str_rule = [str(i % 10) for i in range(size // 10 + 2)]
    str_rule = '....+....'.join(str_rule)[1:size+1]
    return str_leading + str_rule


def verticalrule(size=23, leading=0):
    """Make a vertical rule"""
    str_leading = '\n'.join(['' for x in range(1 + leading)])
    str_rule = '\n'.join(list(rule(size)))
    return str_leading + str_rule


def main():
    """Parse the command line when called as a program"""

    # Handle command line arguments
    parser = argparse.ArgumentParser(description='Print a rule')
    parser.add_argument('size', type=int, nargs='?', help='the size of rule')
    parser.add_argument(
        '-l', '--leading', type=int, default=0, help='number of leading spaces'
    )
    parser.add_argument(
        '-v', '--vertical', action='store_true', help='vertical rule'
    )
    args = parser.parse_args()

    # Pick the size of rule.
    if args.size is None:
        try:
            vsize, size = curses.initscr().getmaxyx()
            curses.endwin()
            args.size = vsize - 1 if args.vertical else size
        except:
            args.size = 23 if args.vertical else 80
        if 0 < args.leading:
            args.size -= args.leading

    # Print rule.
    if args.vertical:
        print(verticalrule(args.size, args.leading))
    else:
        print(rule(args.size, args.leading))


if __name__ == '__main__':
    main()
